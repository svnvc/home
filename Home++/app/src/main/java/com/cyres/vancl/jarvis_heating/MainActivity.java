package com.cyres.vancl.jarvis_heating;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    EditText debugText;
    SpeechRecognizer mspeechRecognizer;
    Intent mSpeechRecognizerIntent;

    //text to speech
    TextToSpeech speaker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initializeSpeaker();
        // svnes code
        editText = findViewById(R.id.editText);
        mspeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.US.toString());
        mspeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onResults(Bundle results) {

                ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (matches !=null){
                    editText.setText(matches.get(0));

                    try {
                        processText(matches.get(0));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (TimeoutException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        });
        findViewById(R.id.button).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_UP:
                        mspeechRecognizer.stopListening();
                        editText.setHint("you will see input here");
                        break;
                    case MotionEvent.ACTION_DOWN:
                        editText.setText("");
                        editText.setHint("listening");
                        mspeechRecognizer.startListening(mSpeechRecognizerIntent);

                        break;
                }
                return false;
            }
        });




    }


    private void processText(String s) throws InterruptedException, ExecutionException, TimeoutException {
        debugText = (EditText) findViewById(R.id.debugText);
        TextView ip = (TextView) findViewById(R.id.input_ip_2);

        Jarviscalls jarvisCall = new Jarviscalls();

        String ipAddress = ip.getText().toString().replace(" ","");


        if (s.contains("heating on")){
            String command = "http://" + ipAddress + ":" + Jarviscalls.PORT + Jarviscalls.TURN_ON;
            Jarviscalls jarviscall_on = new Jarviscalls();
            jarviscall_on.execute(command);
            showResults(jarviscall_on.get(3000,TimeUnit.MILLISECONDS));
            speaker.speak("Heating turned on.",TextToSpeech.QUEUE_FLUSH,null);
        }

        if (s.contains("heating off")){
            String command = "http://" + ipAddress + ":" + Jarviscalls.PORT + Jarviscalls.TURN_OFF;
            jarvisCall.execute(command);
            speaker.speak("Heating turned off",TextToSpeech.QUEUE_FLUSH,null);

        }

        if (s.contains("heating auto")){

            String command = "http://" + ipAddress + ":" + Jarviscalls.PORT + Jarviscalls.TURN_ONAUTO;
            String message = "Heating turned on, will turn off automatically in 20 seconds";

            if (s.contains("seconds")){
                String[] splits = s.split(" ");

                String secondsmsg = "";
                int targetSecs = 0;
                for(int i=0;i<splits.length;i++){
                    try {
                        targetSecs = Integer.parseInt(splits[i]);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    secondsmsg += " // " + splits[i];

                }

                editText.setText(secondsmsg);
                debugText.setText(Integer.toString(targetSecs));

                command = "http://" + ipAddress + ":" + Jarviscalls.PORT + Jarviscalls.TURN_ONAUTO + "/"+Integer.toString(targetSecs);
                message = "Heating turned on, will turn off automatically in " + Integer.toString(targetSecs) + "seconds";
            }





            Jarviscalls jarvis_auto = new Jarviscalls();
            jarvis_auto.execute(command);
            showResults(jarvis_auto.get(3000,TimeUnit.MILLISECONDS).toString());
            speaker.speak(message,TextToSpeech.QUEUE_FLUSH,null);

        }

        if (s.contains("heating boolean") == true){
            debugText.setText("contains heating");
            speaker.speak("oh, you said heating!",TextToSpeech.QUEUE_FLUSH,null);


            asynctask at = new asynctask();

            //at.execute("http://192.168.0.23:5000/jarvis/heating/on");

            String comp = ip.getText().toString() + ":5000/home";
            editText.setText(comp.replace(" ",""));
            at.execute("http://"+ip.getText().toString().replace(" ","")+":5000/home");

            try {
                editText.setText(at.get(3000, TimeUnit.MILLISECONDS));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            }

        } else {
            //debugText.setText("no recognized command");
            debugText.setText(debugText.getText().toString());
        }

        //set text view with received text

    }


    public void showResults(String text) throws TimeoutException{
        editText.setText(text);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeSpeaker(){
        speaker = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

                speaker.setLanguage(Locale.ENGLISH);
                speaker.setSpeechRate((float) 0.8);

            }
        });
    }

    private class asynctask extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... strings) {
            String urlstring = "http://192.168.0.23:5000/home";
            String str = strings[0];
            URL url = null;
            try {
                url = new URL(str);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            URLConnection conn = null;
            try {
                conn = url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = "";
            String result = "";
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                while ((line=br.readLine())!=null){
                    result += line;
                }
                br.close();


                //debugText.setText(result.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result ;
        }



    }

}
