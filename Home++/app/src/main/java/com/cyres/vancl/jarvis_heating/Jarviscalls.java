package com.cyres.vancl.jarvis_heating;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Jarviscalls extends AsyncTask <String, String, String> {


    static public String TURN_ON = "/jarvis/heating/on";
    static public String TURN_OFF = "/jarvis/heating/off";
    static public String TURN_ONAUTO = "/jarvis/heating/onauto";
    static public String HOME = "/home";
    static public String PORT = "5000";


    @Override
    protected String doInBackground(String... strings) {
        String str = strings[0];
        URL url = null;
        try {
            url = new URL(str);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        URLConnection conn = null;
        try {
            conn = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            while ((line=br.readLine())!=null){
                result += line;
            }
            br.close();


            //debugText.setText(result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result ;
    }
}
